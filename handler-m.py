import threading
import dotenv

from clustertools import master_handler


# Thread started for each new connection
def function_spine(con, addr):

    # send cluster-map to worker-handler at connection
    package = (1, addr, 100, handler.cluster_map)
    handler.send_queue.put(package)
    handler.logger.debug(f"SENT INITIAL CLUSTER-MAP TO {addr}")

    while True:

        handler.logger.debug("WAITING FOR PACKAGE")
        package = (prio, dest, code, data) = handler.cmd_receiver(con)
        handler.logger.debug(f"RECEIVED PACKAGE FROM MASTER; CODE:{code}")

        # a form of "spinal cord" for actuation of functions
        if code == 101:
            # update cluster-map
            handler.update_cluster_map()
        elif code == 302:
            pass
        elif code == 998:
            exit()


def main():

    # def globals
    global handler

    # set env constants, env location will vary by distribution!!!
    dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

    handler = master_handler(func_spine=function_spine)

    handler.start_server(host=handler.ipaddress, port=handler.master_port)

    # write the cluster map for program execution
    handler.compose_cluster_map()

    # start modules
    handler.start_modules(master=True)

    # TODO: sysinfo sender
    # define & start necessary parallel threads
    handler.logger.info("STARTING PARALLELL THREADS: cmd_sender")
    t1 = threading.Thread(target=handler.cmd_sender, args=())

    t1.start()
    handler.logger.info("PARALLELL THREADS STARTED")

    # accepts any new worker-handler connection to port
    handler.logger.info("STARTING HANDLER_CONNECTOR FUNCTION")
    handler.handler_connector()

    # TODO: create shutdown process


if __name__ == '__main__':
    main()
