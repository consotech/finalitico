import os
import dotenv
import threading

from clustertools import worker_handler


# receives commands / packages from master-handler
# only parts of cmd_receiver can be in class, because of "function spine"
def function_spine():
    while True:
        handler.logger.debug("WAITING FOR PACKAGE")
        package = (prio, dest, code, data) = handler.cmd_receiver()
        handler.logger.debug(f"RECEIVED PACKAGE FROM MASTER; CODE:{code}")

        # a form of "spinal cord" for actuation of functions
        if code == 100:
            # initial cluster-map
            handler.update_cluster_map(cluster_map=data)

        elif code == 101:
            # updates cluster-map
            handler.update_cluster_map(commit_data=data)

        elif code == 102:
            # start modules
            handler.start_modules()

        elif code == 988:
            pass

        elif code == 989:
            pass

        elif code == 998:
            return

        elif code == 999:
            exit()


# main function, starts all necessary processes
def main():
    # def globals
    global ROOT
    global handler

    # set env constants, env location will vary by distribution!!!
    dotenv.load_dotenv('/home/ubuntu/finalitico/.env')
    ROOT = os.getenv('ROOT')

    # setup worker-handler object
    handler = worker_handler()

    # TODO: sysinfo sender
    # define & start necessary parallel threads
    handler.logger.info("STARTING PARALLELL THREADS: cmd_sender")
    t1 = threading.Thread(target=handler.cmd_sender, args=())

    t1.start()
    handler.logger.info("PARALLELL THREADS STARTED")
    
    # start function_spine function, main func will stop here till exit cmd
    handler.logger.info("STARTING CMD_RECEIVER FUNCTION")
    function_spine()

    # TODO: create shutdown process


if __name__ == '__main__':
    main()
