import datetime
import pandas as pd
import sys
import os
import dotenv

# import custom libs
sys.path.insert(0, f'../../comm/data-API/b_data')
from b_data import b_data as borsdata

dotenv.load_dotenv('../../.env')


# download raw stock price daily for modules in hdf5 file
def raw_stock_price_daily():

    api_data = bdata.fetch_insrtum()

    avl_instrum = pd.DataFrame(api_data["instruments"])
    avl_stock = avl_instrum.loc[avl_instrum["instrument"] == 0]

    compiled_data = pd.DataFrame(
        columns={'insId': int(), 'd': None, 'h': float(),
                 'l': float(), 'c': float(), 'o': float(), 'v': int()},
        index=[])

    for idx, row in avl_stock.iterrows():
        insId = row['insId']

        print(f"DOWNLOADING DATA FOR STOCK INSID: {insId}")

        # fetch data
        raw_data = bdata.stockprice_historic(insId)['stockPricesList']

        # format to dataframe to match systems
        tmp_df = pd.DataFrame(raw_data)
        tmp_df[['h', 'l', 'c', 'o']] = tmp_df[['h', 'l', 'c', 'o']].round(3)
        tmp_df.insert(loc=0, column='insId', value=insId)
        tmp_df['d'] = pd.to_datetime(tmp_df['d']).dt.date

        # reconstruct missing data (NULL)
        if tmp_df.isnull().values.any():
            print(f"\nNAN VALUES DETECTED FOR INSID: {insId}\n")
            tmp_df = tmp_df.interpolate()

        # load to memory
        compiled_data = compiled_data.append(tmp_df, ignore_index=True)
        print("COMPLETED DOWNLOAD")

    print("\nSTORING DATA IN LOCAL HDF5 FILE\n")
    with pd.HDFStore(f'{DEV_ROOT}/modules/tmp/dbtools_dataframes.hdf5') as store:
        store.put('raw_stock_price_daily', compiled_data)
        store.get_storer('raw_stock_price_daily').attrs.metadata = {'updated': datetime.date}
    print("\nDONE")


def raw_avl_instruments():
    print("\n\nDOWNLOADING RAW DATA FOR AVAILABLE INSTRUMENTS")
    api_data = bdata.fetch_insrtum()

    avl_instrum = pd.DataFrame(api_data["instruments"])
    avl_stock = avl_instrum.loc[avl_instrum["instrument"] == 0]

    print("\nSTORING DATA IN LOCAL HDF5 FILE\n")
    with pd.HDFStore(f'{DEV_ROOT}/modules/tmp/dbtools_dataframes.hdf5') as store:
        store.put('raw_avl_instruments', avl_stock)
        store.get_storer('raw_avl_instruments').attrs.metadata = {'updated': datetime.date}
    print("\nDONE")


# enter download function to execute here
def main():

    # raw_stock_price_daily()
    raw_avl_instruments()


if __name__ == '__main__':
    bdata = borsdata(api_key=os.getenv('API_KEY'))
    DEV_ROOT = os.getenv('DEV_ROOT')

    main()
