# ------ LIBS FOR TESTING ------
import logging
import __main__
import os
import dotenv

# ------ LOGGER FOR TESTING: will log to stdout ------
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s [%(levelname)s] %(module)s -> %(funcName)s: %(message)s')

# ------ LOAD ENVIRONMENTAL VARIABLES ------
env_path = './../../.env'
dotenv.load_dotenv(env_path)

# ------ MOD / LIB TO TEST ------


# ------ TEST TO RUN ------
def test():
    pass




if __name__ == '__main__':
    test()