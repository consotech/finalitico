# script used for uploading project files to a test-rig
# some functionality presumes that testrig is running on local lxc containers
# TODO: make compatible with install scripts
# TODO: expand functionality to work outside of lxc containers
import sys
from configparser import ConfigParser
import toml
import dotenv
import os
import subprocess
from subprocess import PIPE
from time import sleep
import multiprocessing

# global conf
dotenv.load_dotenv('../../.env')
DEV_ROOT = os.getenv('DEV_ROOT')


# simple func to execute cmd properly with subprcs
def execute_cmd(cmd, verbose=True):
    prcs = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE)

    if verbose:
        stdout, stderr = prcs.communicate()

        print(stdout)
        print(stderr)


# TODO: fix rsyncignore so files actually are ignored
# used for testing & applying new changes on remote cluster
def upload(cluster_ips: list, usr: str, target: str):
    # upload to cluster machines
    for ip in cluster_ips:
        cmd = ['rsync', '-arP', f'--exclude-from=rsyncignore.txt',
               f'{DEV_ROOT}', f'{usr}@{ip}:{target}']

        execute_cmd(cmd)


# TODO: Pass if it's master to install script
# TODO: testing
# install system on cluster machines
def install_system(cluster_ips: list, usr: str, target: str):
    for ip in cluster_ips:
        # install rsync & xargs upload_rig
        cmd = ['ssh', f'{usr}@{ip}', 'sudo apt install rsync xargs -y']
        execute_cmd(cmd)

        # upload system
        upload(cluster_ips, usr, target)

        # install system
        cmd = ['ssh', f'{usr}@{ip}', f'{target}/finalitico/install/install.sh']
        execute_cmd(cmd)


# TODO: testing
# update requirements on machines
def update_requirements(cluster_ips: list, usr: str, target: str):
    for ip in cluster_ips:

        # install linux-requirements
        cmd = ['ssh', f'{usr}@{ip}',
               f'sudo apt-get install $(grep -vE "^\s*#" {target}/finalitico/install/requirements/linux-packages.txt  | tr "\n" " ")']
        execute_cmd(cmd)

        # install python-requirements
        cmd = ['ssh', f'{usr}@{ip}',
               f'pip3 install -r {target}/finalitico/install/requirements/python-requirements.txt']
        execute_cmd(cmd)


# func starts all handlers on cluster
def start_handlers(cluster_ips : list, usr: str, master_ip: str):

    # restarts all lxc containers
    subprocess.call(['lxc', 'restart', '--all'])

    # sleep so all containers are fully started
    sleep(1.5)

    # start all handlers
    for i, ip in enumerate(cluster_ips):
        if ip == master_ip:
            cmd = [f'bash', '-c', f'ssh {usr}@{ip} /home/{usr}/finalitico/.venv/bin/python3 -u /home/{usr}/finalitico/handler-m.py']
        else:
            cmd = [f'bash', '-c', f'ssh {usr}@{ip} /home/{usr}/finalitico/.venv/bin/python3 -u /home/{usr}/finalitico/handler-w.py']

        execute_cmd(cmd, verbose=False)
        print(f"Started handler on node: {ip}")


# func opens handler logs on cluster
# TODO: functionality to instantly close all terminals
def logs_terminal(cluster_ips : list, usr: str, target: str, master_ip: str, terminal: str):

    # start all handlers
    for i, ip in enumerate(cluster_ips):

        if ip == master_ip:
            cmd = [f'{terminal}', '--title', f'({i})MASTER-HANDLER: {usr}@{ip}', '-e', 'multitail', '-l',
                   f'ssh {usr}@{ip} tail -f /home/{usr}/finalitico/logs/handler-m.py.log']
        else:
            cmd = [f'{terminal}', '--title', f'({i})WORKER-HANDLER: {usr}@{ip}', '-e', 'multitail', '-l',
                   f'ssh {usr}@{ip} tail -f /home/{usr}/finalitico/logs/handler-w.py.log']

        p = multiprocessing.Process(target=execute_cmd, args=(cmd,))
        p.start()
        print(f"Opening logs for node: {ip}")


def main():
    # # configuration upload_rig.ini
    # GENERAL
    config = ConfigParser()
    config.read('upload_rig.ini')

    do_upload = config.getboolean('GENERAL', 'upload')
    update_req = config.getboolean('GENERAL', 'update_requirements')
    do_install = config.getboolean('GENERAL', 'install')

    machinefile_path = config.get('GENERAL', 'machinefile_path')

    target_dir = config.get('GENERAL', 'target_dir')

    # GUI
    show_logs = config.getboolean('GUI', 'show_logs')

    handler_logs = config.getboolean('GUI', 'handler_logs')
    module_logs = config.getboolean('GUI', 'module_logs')

    terminal_eml = config.get('GUI', 'terminal_eml')

    # SCRIPT
    activate_handlers = config.getboolean('SCRIPT', 'start_handlers')

    # # configuration handler.toml
    # CLUSTER
    machine_conf = toml.load(machinefile_path)

    master_ip = machine_conf['CLUSTER']['master'][0]
    master_port = machine_conf['CLUSTER']['master'][1]

    workers_ip = machine_conf['CLUSTER']['workers']

    machine_ips = [ip for ip in workers_ip]
    machine_ips.insert(0, master_ip)

    clust_usr = machine_conf['CLUSTER']['user']

    # execute program
    if do_upload:
        print("\n\nUPLOADING NEW SYSTEM-FILES TO CLUSTER NODES\n")
        upload(cluster_ips=machine_ips, usr=clust_usr, target=target_dir)
    elif do_install:
        print("\n\nINSTALLING FINALITICO SYSTEM ON CLUSTER\n")
        install_system(cluster_ips=machine_ips, usr=clust_usr, target=target_dir)

    if update_req:
        print("\n\nUPDATING PACKAGES ON CLUSTER TO MATCH SYSTEM REQUIREMENTS\n")
        update_requirements(cluster_ips=machine_ips, usr=clust_usr, target=target_dir)

    if activate_handlers:
        print("\n\nSTARTING HANDLERS ON ALL MACHINES")
        start_handlers(cluster_ips=machine_ips, usr=clust_usr, master_ip=master_ip)

    if show_logs:
        print(f"\n\nOPENING LOGS IN {terminal_eml} - TERMINAL!")
        logs_terminal(cluster_ips=machine_ips, usr=clust_usr, target=target_dir,
                      master_ip=master_ip, terminal=terminal_eml)

    print("\n\nCLUSTER CONFIGURATION COMPLETE")
    exit()


if __name__ == '__main__':
    main()
