#!/bin/bash

echo -e "\nInstalling the Finalitico-system on: $(hostname)"

# get flag args
while getopts m:d: flag
do
    # shellcheck disable=SC2220
    case "${flag}" in
        m) master=${OPTARG};;
        d) root_dir=${OPTARG};;
    esac
done

# install upload_rig necessary for installation & execution
sudo apt install xargs

# install linux required packages
xargs sudo apt install <$root_dir/finalitico/requirements/linux-packages.txt -y

# setup of python environment
mkdir $root_dir/finalitico/.venv
python3 -m venv $root_dir/finalitico/.venv

# install python libs
$root_dir/finalitico/.venv/bin/pip3 install -r $root_dir/finalitico/requirements/python-requirements.txt

# TODO: add automatic conf
# install mariadb-server on master
if [ ${master,,} = true ]
then
    echo -e "\nMachine will be configured as master"

    # fetch details for db install
    echo -e "\nInstalling mariadb-server on master"
    echo -e "Please enter machines user: "
    read MACHINE_USER

    echo -e "Please enter machine database password: "
    read MACHINE_DBPASS

    echo -e "Please enter database root-user password: "
    read ROOT_DBPASS

    # start db install script
    sudo /bin/bash $root_dir/finalitico/install/mariadb_install.sh -d $root_dir -u $MACHINE_USER -p $MACHINE_DBPASS -r $ROOT_DBPASS
fi

# TODO: Add system check routine

echo -e "\nFinalitico-system installed successfully"
