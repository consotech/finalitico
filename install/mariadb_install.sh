#!/bin/bash


echo -e "\nInstalling and configuring mariadb..."

# get flag args
while getopts d:u:p:r: flag
do
    # shellcheck disable=SC2220
    case "${flag}" in
        d) root_dir=${OPTARG};;
        u) machine_user=${OPTARG};;
        p) passw=${OPTARG};;
        r) DBROOTPASS=${OPTARG};;
    esac
done

######## INSTALLING MARIADB ##############################
echo -e -e "\nInstalling mariaDb from official repos"
sudo apt update
sudo apt install mariadb-server -y
sudo systemctl enable mariadb
sudo systemctl start mariadb
######################################################################

######## SECURE INSTALLATION OF MARIADB ##############################
echo -e "\nSecuring mariaDB installation"
# Make sure that NOBODY can access the server without a password
sudo mysql -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('$DBROOTPASS')"
# Kill the anonymous users
sudo mysql -e "DROP USER IF EXISTS ''@'localhost'"
# Because our hostname varies we'll use some Bash magic here.
sudo mysql -e "DROP USER IF EXISTS ''@'$(hostname)'"
# Kill off the demo database
sudo mysql -e "DROP DATABASE IF EXISTS test"
#####################################################################

######## CREATE DATABASES ###########################################
# creates all databases used by script
echo -e "\nCreating default databases"
sudo mysql -e "CREATE DATABASE IF NOT EXISTS instrumentsdb"
sudo mysql -e "CREATE DATABASE IF NOT EXISTS poolsdb"
sudo mysql -e "CREATE DATABASE IF NOT EXISTS simulationsdb"
#####################################################################

######## CREATE TABLES ##############################################
# Setup tables with schemas
echo -e "\nSyncing table schemas to databases"
sudo mysql instrumentsdb < $root_dir/finalitico/defaults/db_schemas/instrumentsdb_schema.sql
sudo mysql poolsdb < $root_dir/finalitico/defaults/db_schemas/poolsdb_schema.sql
sudo mysql simulationsdb < $root_dir/finalitico/defaults/db_schemas/simulationsdb_schema.sql
#####################################################################

if [ -s $root_dir/finalitico/install/machines.txt ]
then
     echo -e "\nMachine text-file contains machines\n"
else
     echo -e "\nMachine text-file is not properly configured!!!"
     echo -e "Please add machine ip:s to machines.txt, done? y/n\n"
     read ANSWER
fi


######### CREATE USERS & ADD PRIVILEGES ##############################
while read IP; do

  sudo mysql -e "CREATE USER IF NOT EXISTS '$machine_user'@'$IP' IDENTIFIED BY '$passw'"
  sudo mysql -e "GRANT ALL PRIVILEGES ON instrumentsdb.* TO '$machine_user'@'$IP'"
  sudo mysql -e "GRANT ALL PRIVILEGES ON poolsdb.* TO '$machine_user'@'$IP'"
  sudo mysql -e "GRANT ALL PRIVILEGES ON simulationsdb.* TO '$machine_user'@'$IP'"
  echo -e "Privileges configured for $machine_user@$IP"

done <$root_dir/finalitico/install/machines.txt
######################################################################
