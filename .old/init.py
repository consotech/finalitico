import os
import shutil
import toml
import sys
import dotenv
import subprocess
from subprocess import PIPE
from filecmp import dircmp
from clustertools import cluster


# checks if .env file exists & helps configure it
def check_env():

    if not os.path.exists('../.env'):
        # create necessary files for program execution
        shutil.copyfile(src='../.defaults/env_default.txt', dst='../.env')

        while input('Please enter all variable information into .env file, done y/n:').lower() != 'y':
            pass

    # TODO:
    #  check if content is accurate
    #  test content, such as api call


def check_defaults(files):
    conf_files = ['cluster.toml', 'handler.toml', 'cluster_map.toml']

    # check if all needed files exists & are configured
    for file in files:
        if not os.path.exists(f'./config/{file}'):
            shutil.copyfile(src=f'./.defaults/{file}',  dst=f'./config/{file}')

            # does file need configuration by user
            if file in conf_files:
                while input(f'Please enter configuration in {file}, done y/n:').lower() != 'y':
                    pass

                # TODO:
                #  check if content is accurate
                #  test content, such as func or if None type call


def select_script() -> str:
    script = input('\nScript to run on cluster: ')

    while not os.path.exists(f'{script}'):
        print("'script' file does not exist, make sure it's in current directory.")
        script = input('Please input script to run on cluster: ')

    # TODO:
    #  check script compatibility

    return script

def sys_check():

    # check if environmental file
    check_env()

    # check if other necessary files
    check_defaults(['cluster.toml', 'default_logging.ini',
                    'handler.toml', 'cluster_map.toml'])


def main():
    dotenv.load_dotenv()
    print(os.getenv('ROOT'))

    print('Starting cluster-initialization program')

    print('execution system check function')
    sys_check()

    print('loading env variables')
    dotenv.load_dotenv('../.env')

    print('creating cluster object')
    my_cluster = cluster(cluster_conf='./config/cluster.toml', verify=True,
                           perm_verbose=True)

    # TODO: some way to fast differ cluster computers files over ssh

    print('Syncing all cluster machines')
    my_cluster.upload_all(verbose=True, ROOT='/home/elias/finalitico')

    script = select_script()

    print('Starting all handlers')
    my_cluster.start_master_handler()
    my_cluster.start_all_worker_handlers()

    while input('Kill program, y/n?').lower() != 'y':
        pass


if __name__ == '__main__':
    main()