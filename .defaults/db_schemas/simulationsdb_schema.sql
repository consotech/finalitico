-- MySQL dump 10.19  Distrib 10.3.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: simulationsdb
-- ------------------------------------------------------
-- Server version	10.3.31-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pool_reg`
--

DROP TABLE IF EXISTS `pool_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_reg` (
  `plid` int(11) NOT NULL AUTO_INCREMENT,
  `simid` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `start_amount` int(11) NOT NULL,
  `dist_stocks` tinyint(4) NOT NULL,
  `dist_avl` tinyint(4) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`plid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `simulations`
--

DROP TABLE IF EXISTS `simulations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simulations` (
  `simid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `started` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) DEFAULT NULL,
  `execution_time` time DEFAULT NULL,
  `start_amount` int(11) NOT NULL,
  `total_pat` decimal(10,3) DEFAULT NULL,
  `yearly_avg` decimal(10,3) DEFAULT NULL,
  `monthly_avg` decimal(10,3) DEFAULT NULL,
  `daily_avg` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`simid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trade_reg`
--

DROP TABLE IF EXISTS `trade_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade_reg` (
  `simid` int(11) NOT NULL,
  `plid` int(11) NOT NULL,
  `trid` int(11) NOT NULL AUTO_INCREMENT,
  `glbid` int(11) NOT NULL,
  `isin` varchar(50) NOT NULL,
  `chance` decimal(5,3) DEFAULT NULL,
  `b_price` decimal(10,3) DEFAULT NULL,
  `s_price` decimal(10,3) DEFAULT NULL,
  `vol` int(11) DEFAULT NULL,
  `pbt` decimal(10,3) DEFAULT NULL,
  `pat` decimal(10,3) DEFAULT NULL,
  `tax` decimal(5,3) DEFAULT NULL,
  `gain` decimal(10,3) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `bought` datetime DEFAULT NULL,
  `sold` datetime DEFAULT NULL,
  PRIMARY KEY (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trades`
--

DROP TABLE IF EXISTS `trades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trades` (
  `simdid` int(11) NOT NULL,
  `plid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `type` varchar(5) NOT NULL,
  `glbid` int(11) NOT NULL,
  `vol` int(11) NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `st_extime` datetime NOT NULL,
  `ed_extime` datetime NOT NULL,
  PRIMARY KEY (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-06 13:42:37
