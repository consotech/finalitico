import threading
import queue
import pickle
from _thread import *
import dotenv
import time
import sys

sys.path.insert(0, f'/home/ubuntu/finalitico')
sys.path.insert(0, f'/home/ubuntu/finalitico/modules/test')
dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

from primenumb import primetools
from clustertools import header_module

# 301: data to prime calc
# 302: compiled primenumbs into list
# 901: module disconnecting


def cmd_receiver(con):
    while True:

        package = (prio, dest, code, data) = module.cmd_receiver(con)

        # a form of "spinal cord" for actuation of functions
        if code == 301:
            pass
        elif code == 302:
            prime_manager.add_to_storage(data)
        elif code == 901:
            pass

def cmd_sender():

    HEADERSIZE = 10

    while True:
        package = (prio, dest, code, data) = QUEUE.get()
        data = pickle.dumps(package)
        data = bytes(f"{len(data):<{HEADERSIZE}}", 'utf-8')+data
        con = SLAVES[dest]

        con.send(data)


def schedual():

    # wait till all slaves are conected
    while len(SLAVES) != 3:
        print(f'slaves connected: {len(SLAVES)}', end="\r")

    print('timer starting')
    start = time.time()

    # create object to handle primenumbs
    global prime_manager
    prime_manager = primetools(min=3)

    i, j = 40000, 0

    if len(SLAVES) == 1:

        for addr in SLAVES:
            l = [i for i in range(0, 120000)]
            QUEUE.put((1, addr, 301, l))
            print(f'slave {addr} data on QUEUE')
            print(f"slave: {addr} got numbers {0} to {120000}")

    else:
        # distribute all data
        for addr in SLAVES:
            l = [i for i in range(j, i)]
            QUEUE.put((1, addr, 301,l))
            print(f'slave {addr} data on QUEUE')
            print(f"slave: {addr} got numbers {j} to {i}")
            i += 40000
            j += 40000

    print('data distributed')

    # wait till all data has arrived
    while prime_manager.recieved_packages < prime_manager.min_packages:
        #print("Waiting for packages!")
        pass
    print('everything has arrived!')

    end = time.time()

    # print(f' All prime numbers: {prime_manager.all_primes}')
    print(f'Total amount of primes: {len(prime_manager.all_primes)}')
    print(f'Time for execution: {end-start}')

def main():
    global QUEUE
    global SLAVES
    global prime_manager
    global module
    QUEUE = queue.PriorityQueue()
    SLAVES = {}

    # module object setup
    module = header_module(dir='test')
    # mod_logger = module.get_logger()

    # start server
    module.start_server()

    t1 = threading.Thread(target=cmd_sender, args=())
    t2 = threading.Thread(target=schedual, args=())

    t1.start()
    t2.start()

    while True:
        con, addr = module.socket.accept()
        print(f"New connection: {addr}")
        start_new_thread(cmd_receiver, (con,))
        SLAVES[addr] = con



if __name__ == '__main__':
    main()
