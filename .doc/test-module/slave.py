import sys
import queue
import threading
import dotenv

# add paths for importing custom modules
sys.path.insert(0, f'/home/ubuntu/finalitico')
sys.path.insert(0, f'/home/ubuntu/finalitico/modules/test')
dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

from primenumb import calc
from clustertools import slave_module


# function for calculating & putting primes on queue
def calc_and_send(numbs):
    primes = calc.calculate_primes(numbs)
    print('Done calculating')

    package = (1, 'master', 302, primes)
    sendQueue.put(package)
    print('QUEUE package put')


# receives commands / packages from header
# only parts of cmd_receiver can be in class, because of "function spine"
def cmd_receiver():
    while True:

        package = (prio, dest, code, data) = module.cmd_receiver()

        # a form of "spinal cord" for actuation of functions
        if code == 301:
            calc_and_send(data)

def main():
    # setup global variables
    global module
    global sendQueue

    # # module object setup
    # create queue for communication of packages with cmd_sender
    sendQueue = queue.PriorityQueue()

    # initialize module object
    module = slave_module(dir='test')
    # mod_logger = module.get_logger()
    config = module.load_config()

    # connect module to header module socket
    module.con_header()

    # # start parallel threads
    # start default cmd_sender from module class
    t1 = threading.Thread(target=module.cmd_sender, args=(sendQueue,))

    t1.start()

    # start receiving commands / packages
    cmd_receiver()


if __name__ == '__main__':
    main()
