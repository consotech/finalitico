import os
import mysql.connector as mq
import sys
import datetime

# add paths & import custom classes
HOME = os.path.expanduser("~")
sys.path.insert(0, f'{HOME}/finalitico/comm/data-API/b_data')
sys.path.insert(0, f'{HOME}/finalitico/hmodules/dbmanager')
sys.path.insert(0, f'{HOME}/finalitico/hmodules/pools')


class reality:
    pass


class simulation:

    def __init__(self):
        # database setup connection
        self.pooldb = mq.connect(database=os.getenv('SIMULATIONSDB'), host=os.getenv('DBHOST'),
                                 user=os.getenv('DBUSER'), password=os.getenv('DBPASS'))
        self.insdb = mq.connect(database=os.getenv('INSTRUMENTDB'), host=os.getenv('DBHOST'),
                                user=os.getenv('DBUSER'), password=os.getenv('DBPASS'))
        self.poolcur = self.pooldb.cursor()
        self.inscur = self.insdb.cursor()

    class tools:

        def map_pools(self):

            query = '''
                        SELECT * FROM 
                    '''


            pass

        def connect_pool(self):
            pass

        def reset_pools(self):
            pass

        def create_pool(self):
            pass

    class pool:

        def __init__(self, simid, plid):
            simulation.__init__()

            self.id = plid
            self.simid = simid

        # buys specific stock at specific date, returns if successful
        def buy_stock(self, date: datetime.date, glbid: int, chance: float) -> bool:
            pass

        def sell_stock(self, date: datetime.date, trid: int):
            pass

        def get_info(self):
            pass

        def lock(self):
            pass

        def destroy(self):
            pass

