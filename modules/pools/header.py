import sys
import dotenv
import os
import threading
import sys

sys.path.insert(0, f'/home/ubuntu/finalitico')

from clustertools import header_module


# Thread started for each new connection
def function_spine(con, addr):
    while True:

        module.logger.debug("WAITING FOR PACKAGE")
        package = (prio, dest, code, data) = module.cmd_receiver(con)
        module.logger.debug(f"RECEIVED PACKAGE FROM MODULE: {addr}; CODE:{code}")

        # a form of "spinal cord" for actuation of functions
        if code == 101:
            # update cluster-map
            module.update_cluster_map(cluster_map=data, handler=False, ROOT=module.ROOT)
        elif code == 302:
            pass


def schedule():






    pass


def main():
    # def globals
    global module

    # set env constants, env location will vary by distribution!!!
    dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

    # init module object
    module = header_module(dir='pools', func_spine=function_spine)

    # start server functionality
    module.start_server()

    # define & start necessary parallel threads
    t1 = threading.Thread(target=module.cmd_sender, args=())

    t1.start()

    # accept any new module connections
    module.slave_connector()

    # TODO: shutdown functionality


if __name__ == '__main__':
    main()
