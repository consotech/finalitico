import os
import mysql.connector as mq
import sys
from clustertools import module
import datetime

# add paths & import custom classes
HOME = os.path.expanduser("~")
sys.path.insert(0, f'{HOME}/finalitico/comm/data-API/b_data')
sys.path.insert(0, f'{HOME}/finalitico/hmodules/dbmanager')
sys.path.insert(0, f'{HOME}/finalitico/hmodules/pools')


class pool_sim:
    def __init__(self):

        # setup module before use
        self.mod = module(dir='pools')
        self.logger = self.mod.get_logger()
        self.conf_dict = self.mod.load_config()

        # database setup connection
        self.simdb = mq.connect(database=os.getenv('SIMULATIONSDB'), host=os.getenv('DBHOST'),
                             user=os.getenv('DBUSER'), password=os.getenv('DBPASS'))
        self.insdb = mq.connect(database=os.getenv('INSTRUMENTDB'), host=os.getenv('DBHOST'),
                             user=os.getenv('DBUSER'), password=os.getenv('DBPASS'))
        self.simcur = self.simdb.cursor()
        self.inscur = self.insdb.cursor()

        self.logger.debug("Initialization of pool control object complete!")

    def buy_stock(self, date: datetime.date, glbid: int, chance: float) -> bool:

        # Fetch data on stock
        sql = f'''
                  SELECT  isin FROM stock_reg where glbId = {glbid}
               '''
        self.inscur.execute(sql)
        isin = self.inscur.fetchone()

        # Fetch price for selected date
        sql = f'''
                  SELECT * FROM stock_hprice WHERE glbId = {glbid} AND date = '{date}'
               '''
        self.inscur.execute(sql)
        prices = self.inscur.fetchone()
        print(prices)

        #
        # # record order success in database
        # sql = f'''
        #         INSERT INTO orders(1, {glbid}, {pltid}, {trid}, {insid}, {isin}, {e_vol}, {r_vol},
        #         {e_price}, {r_price}, {gain}, {chance}, {datetime.datetime.now()}, )
        #         VALUES()
        #
        #       '''

    def sell_stock(self, date, glbid):
        pass

    def update_pool(self):
        pass

    # FUnction creates new asset - pool
    def create_pool(self, name: str, simid: int, start_amount: int, dist_stock, dist_avl) -> int:
        self.logger.debug(" >>> STARING POOL CREATION PROCESS <<<")
        self.logger.debug(" >> CREATING NEW ASSET POOL <<")
        self.logger.debug(f" > Name: {name}, Simulation id: {simid}, Amount: {start_amount}")

        # creates new pool register entry for new pool
        values = (simid, name, start_amount, dist_stock, dist_avl)
        query = f'''
                    INSERT INTO pool_reg (simid, name, start_amount, dist_stocks, dist_avl) 
                    VALUES (%s, %s, %s, %s, %s);     
                '''
        self.cursor.execute(query, values)
        self.db.commit()
        self.logger.debug(" >> POOL CREATED <<\n")

        # Creates a unique table for asset-pool, allocated for "currently owned" stock
        query = f'''
                    SELECT plid FROM pool_reg WHERE simid = {simid} ORDER BY plid DESC LIMIT 1;
                '''
        self.cursor.execute(query)
        plid = self.cursor.fetchall()

        self.logger.debug(" >> CREATING NEW ASSET TABLE <<")
        self.asset_table = f'assets_{plid[0][0]}'

        query = f'''
                    CREATE TABLE IF NOT EXISTS {self.asset_table} (
                    nr INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    trdid INT,
                    name varchar(50),
                    glbid INT NOT NULL,
                    instrument INT NOT NULL,
                    volume INT NOT NULL,
                    price decimal(10,3),
                    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    UNIQUE(trdid)
                    );
                '''
        self.cursor.execute(query)
        self.db.commit()
        self.logger.debug(" >> ASSET TABLE CREATED <<")

        self.logger.debug(" >>> POOL CREATION PROCESS COMPLETE <<<")
        return plid[0][0]

    def connect_pool(self, plid):
        self.asset_table = f'assets_{plid}'
