import sys
import dotenv
import os
import threading
import sys

sys.path.insert(0, f'/home/ubuntu/finalitico')

from clustertools import slave_module


# receives commands / packages from header-module
# only parts of cmd_receiver can be in class, because of "function spine"
def function_spine():
    while True:

        module.logger.debug("WAITING FOR PACKAGE")
        package = (prio, dest, code, data) = module.cmd_receiver()
        module.logger.debug(f"RECEIVED PACKAGE FROM HEADER-MODULE: {module.header_ip}:{module.header_port}; CODE:{code}")

        # a form of "spinal cord" for actuation of functions
        if code == 101:
            # update cluster-map
            module.update_cluster_map(cluster_map=data, handler=False, ROOT=module.ROOT)
        elif code == 302:
            pass


def schedule():
    pass


def main():
    # def globals
    global module

    # set env constants, env location will vary by distribution!!!
    dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

    # init module object
    module = slave_module(dir='dbmanager', func_spine=function_spine)

    # connect to header module
    module.con_header()

    # define & start necessary parallel threads
    t1 = threading.Thread(target=module.cmd_sender, args=())

    t1.start()

    # start function_spine function, main func will stop here till exit cmd
    function_spine()

    # TODO: shutdown functionality


if __name__ == '__main__':
    main()