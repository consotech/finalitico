import dotenv
import threading
import sys

# custom libraries
sys.path.insert(0, f'/home/ubuntu/finalitico')
from clustertools import header_module
from dbtools import instrumentsdb


# TODO: add spine functionality
# Thread started for each new connection
def function_spine(con, addr):
    while True:

        module.logger.debug("WAITING FOR PACKAGE")
        package = (prio, dest, code, data) = module.cmd_receiver(con)
        module.logger.debug(f"RECEIVED PACKAGE FROM MODULE: {addr}; CODE:{code}")

        # a form of "spinal cord" for actuation of functions
        if code == 101:
            # update cluster-map
            module.update_cluster_map()

        # # 310-319: instrumentsdb specific operations
        # update all tables in database
        elif code == 310:

            pass
        # update table stock_reg
        elif code == 311:

            pass
        # update table stock_price_d
        elif code == 312:

            pass


# TODO: schedule function for db manager
def main():
    # def globals
    global module

    # set env constants, env location will vary by distribution!!!
    dotenv.load_dotenv('/home/ubuntu/finalitico/.env')

    # init module object
    module = header_module(dir='dbmanager', func_spine=function_spine)

    # start server functionality
    module.start_server()

    # define & start necessary parallel threads
    t1 = threading.Thread(target=module.cmd_sender, args=())

    t1.start()

    # accept any new module connections
    module.slave_connector()

    # TODO: shutdown functionality


if __name__ == '__main__':
    main()
