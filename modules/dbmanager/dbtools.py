# TODO: correct logging + see over needed / not needed logging
# TODO: make functions work over multiple nodes
import logging
import os
import sys
import pandas
import pandas as pd
import datetime
import configparser
from dateutil.relativedelta import relativedelta
import sqlalchemy


# general database class
class database:
    def __init__(self, logger, database, tmp_dir):
        # logger, env variables
        self.logger = logger
        self.tmp_dir = tmp_dir
        self.ROOT = os.getenv('ROOT')

        # import custom libs
        sys.path.insert(0, f'{self.ROOT}/comm/data-API/b_data')
        from b_data import b_data

        # # config
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        # global / every class instance config settings
        self.chunk_size = self.config.getint('GENERAL', 'chunk_size')

        # API-sources setup
        self.bdata = b_data(api_key=os.getenv('API_KEY'))

        # database connection
        self.engine = sqlalchemy.create_engine(
            f"mysql+mysqlconnector://{os.getenv('DBUSER')}:{os.getenv('DBPASS')}@{os.getenv('DBHOST')}/{database}", echo=False)
        self.con = self.engine.connect()


# database class for instrument database
# TODO: add compare function to validate all data
class instrument(database):
    def __init__(self, logger, tmp_dir):
        database.__init__(self, logger=logger, database='instrument', tmp_dir=tmp_dir)

        # config
        self.dataset_max_price = self.config.getfloat('INSTRUMENT', 'max_dataset_price')
        self.dataset_max_volume = self.config.getfloat('INSTRUMENT', 'max_dataset_volume')

    # fetches dataframe of stocks available, ignoring non-activated
    def __get_active_stocks(self) -> pandas.DataFrame():
        avl_stocks = pd.read_sql(sql='''SELECT insId FROM stock_reg where active = 1''', con=self.con)

        self.logger.debug(" > fetched list of available stocks from mysql database")

        return avl_stocks

    # update dictionary with daily stock prices
    def __get_stock_prices_daily(self, avl_stocks: pandas.DataFrame(), backup_dataset: str = None) \
            -> pandas.DataFrame():

        # today's date to handle files
        date_today = datetime.date.today()

        # check for and return up-to-date datasets
        if backup_dataset:
            with pd.HDFStore(f'{self.tmp_dir}/dbtools_dataframes.hdf5') as store:
                try:
                    metadata = store.get_storer(backup_dataset).attrs.metadata

                    # if backup-dataset is up-to-date
                    if metadata['updated']: # TODO: TEMPRORARY DEVELOPMENT SETTING  == date_today:
                        self.logger.debug("FETCHING DATA FROM HDF5 FILE")
                        return store[backup_dataset]

                # if dataset is outdated continue & report
                except KeyError as e:
                    pass

        # dataframe for all dataframes of stock daily prices
        compiled_dataframe = pd.DataFrame(
            columns={'insId': int(), 'd': None, 'h': float(),
                     'l': float(), 'c': float(), 'o': float(), 'v': int()},
            index=[])

        # fetch updated stock prices for last 20 years for all avl. stocks
        for idx, row in avl_stocks.iterrows():
            insId = row['insId']

            self.logger.debug(f"FETCHING UPTODATE STOCK-PRICE-DAILY: insId {insId}")

            # fetch data
            raw_data = self.bdata.stockprice_historic(insId)['stockPricesList']

            # format to dataframe to match systems
            tmp_df = pd.DataFrame(raw_data)
            tmp_df[['h', 'l', 'c', 'o']] = tmp_df[['h', 'l', 'c', 'o']].round(3)
            tmp_df.insert(loc=0, column='insId', value=insId)
            tmp_df['d'] = pd.to_datetime(tmp_df['d']).dt.date

            # reconstruct missing data (NULL)
            if tmp_df.isnull().values.any():
                self.logger.debug(
                    f"Nan values detected in dataframe insId: {insId}, interpolating replacements!!!")
                tmp_df = tmp_df.interpolate()

            # load to memory
            compiled_dataframe = compiled_dataframe.append(tmp_df, ignore_index=True)
            self.logger.debug(f"{insId} STOCK-PRICE-DAILY SAVED IN MEMORY")

        # mark dataframe updated today
        compiled_dataframe.updated = date_today

        if backup_dataset:
            # dump to json in tmp for use between sessions
            with pd.HDFStore(f'{self.tmp_dir}/dbtools_dataframes.hdf5') as store:
                store.put(backup_dataset, compiled_dataframe)
                store.get_storer(backup_dataset).attrs.metadata = {'updated': compiled_dataframe.updated}

        return compiled_dataframe

    # TODO: check if dates new updated prices has dropped
    def updateTable_stock_price_daily(self):

        self.logger.debug("UPDATING DAILY STOCK-PRICES")

        # fetch avl stocks
        avl_stocks = self.__get_active_stocks()

        # update stock prices in memory
        active_stock_price_daily = self.__get_stock_prices_daily(avl_stocks=avl_stocks,
                                                                 backup_dataset='stock_price_daily')

        # get latest accessible date (data from API)
        limit = self.config.getint('INSTRUMENT', 'year_limit')
        date_today = datetime.date.today()
        limit_date = date_today - relativedelta(years=limit)

        # fetch all stocks in stock_price_daily and their date entries
        date_entries = pd.read_sql(sql=f'''SELECT insId, d from stock_price_daily where d >='{limit_date}' '''
                                   , con=self.con)

        # format to match database format TODO: Possibly useless code, testing
        active_stock_price_daily['d'] = pd.to_datetime(active_stock_price_daily['d']).dt.date

        # find differences, e.g. rows not in MYSQL-server
        stock_updates = pd.concat([active_stock_price_daily, date_entries]).drop_duplicates(keep=False)

        # upload updates to MYSQL-Server table
        # TODO: COULD UPLOAD TO DATABASE WITH THIS, BUT FOR SOME REASON FREEZES AND REMOVES TABLE FROM DATABASE
        self.logger.debug("UPLOADING TO DATABASE")

        stock_updates.to_sql(name='stock_price_daily', con=self.con, if_exists='replace', index=False, method='multi',
                             chunksize=self.chunk_size)

    # TODO: som kind of global index of system resources, for safe use / recalibration / api usage
    # TODO: add limited memory functionality, save data to disc
    # TODO: dump dictionary to tmp for others to use
    # TODO: add reroute functionality (out outside of function)
    # TODO: switch to dataframes
    # func updates data in stock_reg
    def updateTable_stock_reg(self):
        self.logger.debug("UPDATING STOCK-REGISTER TABLE")

        # # fetch avl instrument from bdata
        # api_data = self.bdata.fetch_insrtum()
        # self.logger.debug(f"RECEIVED API INSTRUMENT DATA")
        #
        # # sorting; assets such as pref-stocks, omx30, etc are accessible with other id
        # avl_instrum = pd.DataFrame(api_data["instrument"])
        # avl_stock = avl_instrum.loc[avl_instrum["instrument"] == 0]
        # TMP SOLUTION WHILE DEVELOPING
        self.logger.debug("FETCHING DATA FROM HDF5")
        with pd.HDFStore(f'{self.tmp_dir}/dbtools_dataframes.hdf5') as store:
            avl_stock = store['raw_avl_instrument']

        # gets date of today to handle tmp files
        date = datetime.date.today()

        # # fetch stock prices daily
        # stock_price_daily = self.__get_stock_prices_daily(avl_stocks=avl_stock, backup_dataset=None, raw=True)
        # TMP SOLUTIONS WHEN DEVELOPING
        self.logger.debug("FETCHING DATA FROM HDF5")
        with pd.HDFStore(f'{self.tmp_dir}/dbtools_dataframes.hdf5') as store:
            stock_price_daily = store['raw_stock_price_daily']

        # TODO: check so nothing can crash ram, e.g. giga sized dataset from börsdata

        self.logger.debug("CHECKING DATABASE COMPATIBILITY")

        # Filter stocks for database compatibility or other reasons
        # TODO: exists more efficient ways, please contribute it :(
        # TODO: for some dumb reason can't select all in one, only gives Nans, pls halp
        giant_values = stock_price_daily[stock_price_daily['h'] > self.dataset_max_price]
        giant_values = giant_values.append(stock_price_daily[stock_price_daily['l'] > self.dataset_max_price])
        giant_values = giant_values.append(stock_price_daily[stock_price_daily['v'] > self.dataset_max_volume])

        giant_values = giant_values.drop_duplicates(subset=['insId'], keep='first')

        flagged = giant_values['insId'].tolist()

        for insId in avl_stock['insId']:

            if insId in flagged:
                avl_stock.at[avl_stock.index[avl_stock['insId'] == insId].tolist(), 'active'] = 0
            else:
                avl_stock.at[avl_stock.index[avl_stock['insId'] == insId].tolist(), 'active'] = 1

        self.logger.debug("DATABASE CHECKING SEQUENCE COMPLETE")

        # uploads data to database
        sql02 = ''' 
                           INSERT INTO stock_reg (insId, name, urlName, instrument, isin,
                           ticker, yahoo, sectorId, marketId, branchId, countryId, listingDate, active)
                           VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)

                           ON DUPLICATE KEY UPDATE name = VALUES(name), urlName = VALUES(urlName), 
                           instrument = VALUES(instrument), isin = VALUES(isin),ticker = VALUES(ticker), 
                           yahoo = VALUES(yahoo), sectorId = VALUES(sectorId), marketId = VALUES(marketId), 
                           branchId = VALUES(branchId), countryId = VALUES(countryId), 
                           listingDate = VALUES(listingDate), active = VALUES(active);
                       '''

        # TODO: no longer existing stock needs to be marked non-activated
        self.logger.debug("STARTING UPLOAD OF UPDATED STOCK_REG DATA")
        avl_stock.to_sql(name='stock_reg', con=self.con, if_exists='replace', index=False)
        self.logger.debug("UPLOAD OF STOCK_REG DATA COMPLETE")
