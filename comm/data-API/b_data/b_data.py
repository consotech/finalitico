import requests
from time import sleep


# calss for communication with Börsdatas official API
class b_data:

    # initiation function, sets e.g. api key & .defaults
    def __init__(self, api_key):
        self.api_key = api_key
        self.uri = "https://apiservice.borsdata.se/"
        self.version = 1
        self.root = self.uri + f"v{self.version}/"
        self.params = {
            "authKey": self.api_key,
            "version": self.version,
            'maxYearCount': 20,
            'maxR12QCount': 40,
            'maxCount': 20
        }

        print('starting!')

    # to send api calls to börsdata, allowing access to resources
    def api_call(self, url: str, params: dict) -> dict:
        done = False
        while not done:
            try:
                req = requests.get(self.root + url, params=params)
                if req.status_code != 429:
                    done = True
                else:
                    print(f"too many requests!!! limit exceeded, waiting {0.5} seconds.")
                    sleep(0.5)
            except Exception as e:
                print(f"An error occurred during API-call, trying again! \n Error: {e}")
                pass

        if req.status_code != 200:
            print(f'failed to access target, status-code; {req.status_code}')
            return req
        else:
            # print("successfully accessed resource")

            return req.json()

    # =============================== INSTRUMENT META ===============================
    # get available countries by API
    def get_countries(self):

        url = "countries"
        json_data = self.api_call(url, self.params)

        return json_data

    # get available data on stock-exchanges
    def get_markets(self):

        url = "markets"
        json_data = self.api_call(url, self.params)

        return json_data

    # get available sectors e.g. 'industry' or "raw materials"
    def get_sectors(self):

        url = "sectors"
        json_data = self.api_call(url, self.params)

        return json_data

    # get branches of sectors e.g. under "raw material" sector there's "metals" or "timber"
    def get_branches(self):

        url = "branches"

        json_data = self.api_call(url, self.params)

        return json_data

    # =============================== INSTRUMENTS ===============================
    # fetch available instruments w. insId, isin, etc + names, instrument 1,2,3,4 indicates type
    def fetch_insrtum(self):

        url = "instruments"
        json_data = self.api_call(url, self.params)

        return json_data

    def fetch_UPDinstrum(self):
        url = "instruments/updated"
        json_data = self.api_call(url, self.params)

        return json_data

    # =============================== STOCK-PRICES ===============================

    # fetches historic stockprice data per day in a 20 y timeframe
    def stockprice_historic(self, insId, fr=None, to=None):

        params = dict(self.params)
        if (fr or to) is None:
            pass
        else:
            params['from'] = fr
            params['to'] = to

        url = f"instruments/{insId}/stockprices"

        json_data = self.api_call(url=url, params=params)

        return json_data

    # fetches latest price for all instruments (per day)
    def instrum_last(self):

        url = "instruments/stockprices/last"
        json_data = self.api_call(url, self.params)

        return json_data

    # fetches latest price for all instruments (per day)
    def instrum_date(self, date):
        params = dict(self.params)
        params['date'] = date

        url = "instruments/stockprices/date"
        json_data = self.api_call(url, params=params)

        return json_data

    # =============================== REPORTS ===============================

    def get_report(self, insid, reporttype):

        url = f"instruments/{insid}/reports/{reporttype}"

        params = dict(self.params)
        params['maxCount'] = 40
        json_data = self.api_call(url, params=params)

        return json_data

    def getall_report(self, insid):

        url = f"instruments/{insid}/reports"

        params = dict(self.params)
        params['maxYearCount'] = 20
        params['maxR12QCount'] = 40
        json_data = self.api_call(url, params=params)

        return json_data

    def metadata_report(self):

        url = "instruments/reports/metadata"
        json_data = self.api_call(url, self.params)

        return json_data

    # =============================== KPI ===============================
    # updated kpis
    def kpi(self):

        url = "instruments/kpis/updated"
        json_data = self.api_call(url, self.params)

        return json_data

    # categorized by börsdatas kpiId
    def kpi_summary(self, insid, reporttype):

        url = f"instruments/{insid}/kpis/{reporttype}/summary"
        json_data = self.api_call(url, self.params)

        return json_data

    # takes kpiId & pricetype, further reading on wiki: https://github.com/Borsdata-Sweden/API/wiki/KPI-History
    def kpi_history(self, insid, kpiId, reporttype, pricetype):

        url = f"instruments/{insid}/kpis/{kpiId}/{reporttype}/{pricetype}/history"
        json_data = self.api_call(url, self.params)

        return json_data

    # calculate kpi by year, type and value
    def kpi_calc(self, insid, kpiId, calcGroup, calc):

        url = f"instruments/{insid}/kpis/{kpiId}/{calcGroup}/{calc}"
        json_data = self.api_call(url, self.params)

        return json_data

    # calculate kpi for all instruments
    def kpiall_calc(self, kpiId, calcGroup, calc):

        url = f"instruments/kpis/{kpiId}/{calcGroup}/{calc}"
        json_data = self.api_call(url, self.params)

        return json_data

    # get kpi metadata
    def kpi_metadata(self):

        url = f"instruments/kpis/metadata"
        json_data = self.api_call(url, self.params)

        return json_data
