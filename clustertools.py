import sys
from subprocess import PIPE
import subprocess
import os
import logging
from logging.config import fileConfig
import __main__
import toml
import socket
import pickle
import queue
import threading
import json


# TODO: Move this function to a better place in the code
# recursive-func inserts commit & restructures nested dict, path = 'key.key.key'
def insert_nesteddict(path: str, value, nesteddict: dict) -> dict:
    # path vars for editing dict
    keys = path.split('.')
    r = len(keys) + 1

    # start values, first part to be built
    seed = keys[-1]
    root = value

    # build dictionary from value root, "back to front"
    for i in range(r):
        # skip first (not dict, only value)
        if i == 0:
            pass
        # last iteration, complete nested dict
        elif i + 1 == r:
            nesteddict[seed] = root

            return nesteddict
        # build dictionary level
        else:
            key = keys[-(i + 1)]  # start backwards

            # find complement values, to not affect
            a = nesteddict
            for c in keys:
                a = a[c]
                if c == key:
                    break

            a[seed] = root  # insert new value

            # define new values to iterate through
            root = a
            seed = key


# TODO: Move this class to a better place in the code
# Fake file-like stream object that redirects writes to a logger instance
# credit to Ferry Boender's blog:
# https://www.electricmonk.nl/log/2011/08/14/redirect-stdout-and-stderr-to-a-logger-in-python/
class StreamToLogger(object):
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    # writes the stderr/stdout msg to logfile line by line
    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    # Flush attribute, otherwise gets error
    # TODO: give it it's actual functionality
    def flush(self):
        pass


# TODO: switch to TLS / SSL socket communication, much more secure...
# TODO: add more functions; start_handler, reboot_all_handlers, reboot_handlers, shutdown_all_handlers,
#   shutdown_handler, kill_all_handlers, kill_handler
class cluster:

    # define cluster & construct object
    def __init__(self, cluster_conf, verbose=False, perm_verbose=False, ROOT=False, verify=False):

        # setup object according to config
        self.cluster_conf = toml.load(f'{cluster_conf}')

        self.master = self.cluster_conf['cluster']['master']
        self.workers = self.cluster_conf['cluster']['workers']
        self.usr = self.cluster_conf['cluster']['usr']
        self.dest = self.cluster_conf['cluster']['dest']

        if verify:
            unreachable = self.check_online()
            if unreachable:
                print(f'WARNING: some cluster nodes where not reachable, {unreachable}')

        # other variables
        self.verbose = perm_verbose
        self.handler_prcs = {'master': [], 'worker': []}
        self.main_program = None

        # setup env variables for use in object
        if not ROOT:
            self.ROOT = os.getenv('ROOT')
        else:
            self.ROOT = ROOT

    def upload_all(self, rsync=True, verbose=False, ROOT=False):

        if rsync:
            # prcs_list = []

            # update master
            cmd = ['rsync', '-arP', f"--exclude-from={self.ROOT}/.rsyncignore",
                   f'{self.ROOT}', f'{self.usr}@{self.master}:{self.dest}']

            if ROOT:
                cmd = ['rsync', '-arP', f"--exclude-from={ROOT}/.rsyncignore",
                       f'{ROOT}', f'{self.usr}@{self.master}:{self.dest}']

            prcs = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE)

            stdout, stderr = prcs.communicate()

            if verbose or self.verbose:
                print(stdout)
                print(stderr)

            # update workers
            for ip in self.workers:
                cmd = ['rsync', '-arP', f"--exclude-from={self.ROOT}/.rsyncignore",
                       f'{self.ROOT}', f'{self.usr}@{ip}:{self.dest}']

                if ROOT:
                    cmd = ['rsync', '-arP', f"--exclude-from={ROOT}/.rsyncignore",
                           f'{ROOT}', f'{self.usr}@{ip}:{self.dest}']

                prcs = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE)
                # prcs_list.append(prcs)
                stdout, stderr = prcs.communicate()

                if verbose or self.verbose:
                    print(stdout)
                    print(stderr)

    def upload(self, rsync=True, verbose=False, ROOT=False):

        if rsync:
            # prcs_list = []
            for ip in self.workers:
                cmd = ['rsync', '-arP', f"--exclude-from={self.ROOT}/.rsyncignore",
                       f'{self.ROOT}', f'{self.usr}@{ip}:{self.dest}']

                if ROOT:
                    cmd = ['rsync', '-arP', f"--exclude-from={ROOT}/.rsyncignore",
                           f'{ROOT}', f'{self.usr}@{ip}:{self.dest}']

                prcs = subprocess.Popen(cmd, stdout=PIPE, stderr=PIPE)
                # prcs_list.append(prcs)
                stdout, stderr = prcs.communicate()

                if verbose or self.verbose:
                    print(stdout)
                    print(stderr)

    def check_online(self) -> list:
        unreachable = []

        for ip in self.workers:
            cmd = f'ping -c 1 {ip}'

            if os.system(cmd):
                unreachable.append(ip)

        return unreachable

    def start_master_handler(self, exec=None, SSH=True, verbose=False):

        self.main_program = exec

        # starts all handlers over ssh
        if SSH:
            # start master handler
            handler_exec = f'{self.ROOT}/handler-m.py'
            cmd = ['ssh', f'{self.usr}@{self.master}', f'{self.ROOT}/.venv/bin/python3',
                   f'{handler_exec}', f'{self.main_program}']
            prcs = subprocess.Popen(cmd)

            self.handler_prcs['master'].append(prcs)

    def start_all_worker_handlers(self, SSH=True, verbose=False):

        # starts all handlers over ssh
        if SSH:

            # start all workers handlers
            handler_exec = f'{self.ROOT}/handler-w.py'
            for ip in self.workers:
                cmd = ['ssh', f'{self.usr}@{ip}', f'{self.ROOT}/.venv/bin/python3',
                       f'{handler_exec}']
                prcs = subprocess.Popen(cmd)

                self.handler_prcs['worker'].append(prcs)


# TODO: Rename class to something more descriptive
class general_tools:
    def __init__(self, handler=False, module=False, wdir=None):

        # get env variables
        self.ROOT = os.getenv('ROOT')

        # get logging
        self.logger = self.get_logger(handler=handler, module=module, wdir=wdir)

        # setup socket
        self.ipaddress = self.get_ipaddress()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # setup queues
        self.send_queue = queue.PriorityQueue()

        # setup variables
        self.cluster_map = None

        self.logger.debug("CLUSTER-COMM OBJECT INITIATED")

    def search_tmp(self, file):
        status = os.path.isfile(f"{self.ROOT}/tmp/{file}")

        return status

    # func gives class object their own customized logger
    def get_logger(self, handler=False, module=False, wdir=None) -> logging:

        # chose how to use logging configuration-file
        if handler:
            fileConfig(f'{self.ROOT}/config/default_logging.ini',
                       defaults={'logfilename': f'{self.ROOT}/logs/{os.path.basename(__main__.__file__)}.log'})
        elif module and wdir:
            fileConfig(f'{self.ROOT}/config/default_logging.ini',
                       defaults={'logfilename': f'{wdir}/logs/{os.path.basename(__main__.__file__)}.log'})

        logger = logging.getLogger()

        # redirect all errors (stderr & stdout) to logger
        sys.stdout = StreamToLogger(logger, logging.INFO)
        sys.stderr = StreamToLogger(logger, logging.CRITICAL)  # critical => could crash finalitico system

        return logger


    # parse ip to ip string, found no other way to do it
    def get_ipaddress(self) -> str:
        output = subprocess.check_output("hostname -I", shell=True)
        ip = str(output).split(sep=' ')[0].strip("b'")

        return ip

    def start_server(self, host, port):

        self.socket.bind((host, port))
        self.socket.listen(5)
        self.logger.debug(f"STARTED SERVER-FUNCTIONALITY AT: {host}:{port}")

    # sends cmd to header-module, queue packages are tuples formatted:
    # (PRIO: <int, 0-9>, DEST: ('ip_address', 'port'),
    # CODE: <int, 100 - 999>, DATA: <data_obj, e.g []>)
    def cmd_sender(self, HEADERSIZE=10):
        while True:
            self.logger.debug("WAITING FOR SEND-PACKAGE")
            package = (prio, dest, code, data) = self.send_queue.get()
            send_data = pickle.dumps(package)
            send_data = bytes(f"{len(send_data):<{HEADERSIZE}}", 'utf-8') + send_data

            self.logger.debug(f"SENDING PACKAGE TO DEST: {dest}, SIZE: {HEADERSIZE}")
            self.socket.send(send_data)
            self.logger.debug(f"SENT PACKAGE FOR DEST: {dest}")

    # receive command from another module, eiter by direct connection or specific con
    # needs to exist in a while loop for continuous receiving
    def cmd_receiver(self, con=None):

        if con:
            comm = con
        else:
            comm = self.socket

        # setting up som variables
        HEADERSIZE = 10
        buff_size = 4096
        new_msg = True
        full_msg = b''

        # loop for receiving the whole transmission
        while True:
            msg = comm.recv(buff_size)
            if new_msg:
                self.logger.debug("NEW PACKAGE RECEIVING")
                # check if new message & get msg size (bytes)
                msg_len = int(msg[:HEADERSIZE])
                new_msg = False

            full_msg += msg

            # check if whole message has been received
            if len(full_msg) - HEADERSIZE == msg_len:
                break

        # load message w. pickle from bytes to objects in tuple format: (prio, dest, code, data)
        package = pickle.loads(full_msg[HEADERSIZE:])

        return package


# TODO: function to connect & send data on machine & system to balancer
# TODO: function to shutdown the whole system
class handler(general_tools):
    def __init__(self, func_spine=None):
        general_tools.__init__(self, handler=True)

        # env variables
        self.ROOT = os.getenv('ROOT')

        # passed variables
        self.function_spine = func_spine

        # load configuration file & variables
        self.config = toml.load(f'{self.ROOT}/config/handler.toml')

        self.master_ip = self.config['CLUSTER']['master'][0]
        self.master_port = int(self.config['CLUSTER']['master'][1])

        # cluster-map variables
        self.modules = []
        self.module_index = {}

        self.logger.debug("DEFAULT HANDLER-OBJECT INITIATED")

    # TODO: send update order / signal to slave modules / header modules on local system
    # func to handle first cluster-map or to apply commits to local cluster-map obj & file
    def update_cluster_map(self, commit_data: dict = None, cluster_map: dict = None):
        if cluster_map:  # first cluster-map on system
            self.cluster_map = cluster_map
            with open(fr"{self.ROOT}/cluster_map.json", 'w+') as f:
                json.dump(cluster_map, f)
            self.logger.debug("DUMPED FIRST CLUSTER-MAP TO JSON")

        elif commit_data:  # Update cluster-map w. commit
            # TODO: handle / use metadata
            metadata = commit_data['METADATA']
            changes = commit_data['CHANGES']
            self.logger.debug(f'UPDATING CLUSTER-MAP: version: {metadata["version"]}, created: {metadata["created"]}, '
                              f'origin: {metadata["origin"]}')

            # TODO: recursive func could be made more effective, insert all changes at once
            cluster_map = self.cluster_map
            for change in changes:
                cluster_map = insert_nesteddict(path=change, value=changes[change], nesteddict=cluster_map)

            # update cluster-map & dump in json format
            self.cluster_map = cluster_map
            with open(fr"{self.ROOT}/cluster_map.json", 'w+') as f:
                json.dump(cluster_map, f)
            self.logger.debug(f'CLUSTER-MAP UPDATED')

        else:  # missing variables
            raise TypeError("update_cluster_map missing required positional argument, either: 'commit_data' or "
                            "'cluster_map'. No functionality gets executed")

        # update cluster-map variables
        self.modules = self.cluster_map['MAP']['MACHINES'][self.ipaddress]['modules']

    # starts all modules delegated to machine
    def start_modules(self, master: bool = False):
        self.logger.info(f"STARTING MODULES: {self.modules}")

        # depending on handler
        if master:
            mod_exec = 'header.py'
        else:
            mod_exec = 'slave.py'

        # start every module in list
        for mod in self.modules:
            mod_dir = f'{self.ROOT}/modules/{mod}'

            prcs = subprocess.Popen(['python3', f'{mod_dir}/{mod_exec}'], cwd=mod_dir)

            # index process for easy termination / handling
            self.module_index[mod] = prcs
            self.logger.debug(f"STARTED MODULE; name:{mod} dir:{mod_dir} exec:{mod_exec}")


class master_handler(handler):
    def __init__(self, func_spine=None):
        handler.__init__(self, func_spine)
        self.worker_handlers = {}

        self.logger.debug("MASTER-HANDLER OBJECT INITIATED")

    def handler_connector(self):
        while True:
            con, addr = self.socket.accept()
            thread = threading.Thread(target=self.function_spine, args=(con, addr))
            self.worker_handlers[addr] = (con, thread)
            thread.start()
            self.logger.info(f"HANDLER CONNECTION FROM: {addr}")

    # variation for master-handler, sends via con object
    def cmd_sender(self, HEADERSIZE=10):
        while True:
            # get, read and compile package
            self.logger.debug("WAITING FOR SEND-PACKAGE")
            package = (prio, dest, code, data) = self.send_queue.get()
            send_data = pickle.dumps(package)
            send_data = bytes(f"{len(send_data):<{HEADERSIZE}}", 'utf-8') + send_data

            # sends data to destined connection
            con = self.worker_handlers[dest][0]

            self.logger.debug(f"SENDING PACKAGE TO DEST: {dest}, SIZE: {HEADERSIZE}")
            con.send(send_data)
            self.logger.debug(f"SENT PACKAGE FOR DEST: {dest}")

    # composes cluster-map for communication & work distribution in cluster
    # TODO: actually compose cluster map w. balancer and config files
    # TODO: Currently beta - version, location might change
    def compose_cluster_map(self):

        with open(fr'{self.ROOT}/cluster_map.json') as f:
            self.cluster_map = json.load(f)

        self.logger.info("COMPOSED CLUSTER-MAP")


# TODO: func to shutdown worker-handler
class worker_handler(handler):
    def __init__(self, func_spine=None):
        handler.__init__(self, func_spine)

        # connect to master-handler
        self.con_master_handler()

        self.logger.debug("WORKER-HANDLER OBJECT INITIATED")

    # connect to header module
    def con_master_handler(self):
        self.logger.info("WAITING TO CONNECT TO MASTER-HANDLER")
        while True:
            try:
                self.socket.connect((self.master_ip, self.master_port))
                self.logger.info(f"CONNECTED TO MASTER-HANDLER: {self.master_ip}:{self.master_port}")
                return
            except ConnectionError:
                pass


# TODO: function for transmitting, e.g. broadcasting & sending messages between master & slaves
# TODO: functions for live reloading cluster_map in thread, ping mother process, etc
class module(general_tools):

    def __init__(self, dir: str, func_spine):
        # define system root & module working dir
        self.ROOT = os.getenv('ROOT')
        self.wdir = rf'{self.ROOT}/modules/{dir}'
        self.name = f'{dir}'

        # initiate general_tools
        general_tools.__init__(self, module=True, wdir=self.wdir)

        # get config
        self.config = toml.load(rf'{self.wdir}/config.toml')

        # other variables
        self.func_spine = func_spine
        self.header_ip = str(self.config['header']['con']['ip'])
        self.header_port = int(self.config['header']['con']['port'])

        # execute dir setup
        self.dir_setup()

        self.logger.debug("MODULE OBJECT INITIATED")

    def dir_setup(self):

        # create essential directories
        if not os.path.exists(f'{self.wdir}/logs'):
            os.makedirs(f'{self.wdir}/logs')
            self.logger.debug(f"CREATED LOGS DIRECTORY AT: {self.wdir}/logs")
        if not os.path.exists(f'{self.wdir}/tmp'):
            os.makedirs(f'{self.wdir}/tmp')
            self.logger.debug(f"CREATED TMP DIRECTORY AT: {self.wdir}/tmp")

    # clear system of tmp files
    def clear_tmp(self):
        subprocess.run(f'rm -r {self.wdir}/tmp/*')
        self.logger.debug("CLEARED TMP DIRECTORY")

    # TODO: signal master that new cluster-map is loaded, for safe detach of e.g. modules
    #  som kind of "WORKING" var to wait for cluster-map update & some-kind of lock mechanism for parallel code
    # func to load local cluster-map
    def update_cluster_map(self):
        with open(fr'{self.ROOT}/cluster_map.json') as f:
            self.cluster_map = json.load(f)
        self.logger.debug("RELOADED CLUSTER-MAP")


class slave_module(module):
    def __init__(self, dir: str, func_spine):
        module.__init__(self, dir, func_spine)

    # connect to header module
    def con_header(self):

        self.logger.info("WAITING TO CONNECT TO HEADER-MODULE")
        while True:
            try:
                self.socket.connect((self.header_ip, self.header_port))
                self.logger.info(f"CONNECTED TO HEADER-MODULE: {self.header_ip}:{self.header_port}")
                return
            except ConnectionError:
                pass
            except OSError:
                pass


class header_module(module):
    def __init__(self, dir: str, func_spine):
        module.__init__(self, dir, func_spine)

        # dictionary with all module connections
        self.slaves = {}

    # start server for header module
    def start_server(self):

        # checks for proper configuration
        if self.ipaddress != self.header_ip:
            self.logger.error(
                f"IP ADDRESS IN CONFIG NOT MATCHING!!! CONFIG-IP: {self.header_ip}, REAL: {self.ipaddress}")
            # TODO: update cluster map and send update request to master-handler / update config files

        self.socket.bind((self.header_ip, self.header_port))
        self.socket.listen(5)

    def slave_connector(self):
        while True:
            con, addr = self.socket.accept()
            thread = threading.Thread(target=self.func_spine, args=(con, addr))
            self.slaves[addr] = (con, thread)
            thread.start()
            self.logger.info(f"HANDLER CONNECTION FROM: {addr}")

    # variation for master-handler, sends via con object
    def cmd_sender(self, HEADERSIZE=10):
        while True:
            # get, read and compile package
            self.logger.debug("WAITING FOR SEND-PACKAGE")
            package = (prio, dest, code, data) = self.send_queue.get()
            send_data = pickle.dumps(package)
            send_data = bytes(f"{len(send_data):<{HEADERSIZE}}", 'utf-8') + send_data

            # sends data to destined connection
            con = self.slaves[dest][0]

            # TODO: log size of send_data in bytes, => define what size it is not just "10"
            self.logger.debug(f"SENDING PACKAGE TO DEST: {dest}, SIZE: {HEADERSIZE}")
            con.send(send_data)
            self.logger.debug(f"SENT PACKAGE FOR DEST: {dest}")
